var request = require('superagent');
var assert = require('chai').assert;

describe('Suite one', function(){
  it ('First superagent tests', function(done){
    request.get('http://simple.dev').end(function(res){
      assert.ok(res, 'Response exists.');
      assert.equal(res.status, 200, 'HTTP Status code returned is 200.');
      assert.include(res.text, 'Welcome', 'Body contains string Welcome');
      done();
    });
  });
});
